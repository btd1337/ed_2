# Trabalho de Estrutura de Dados 2

## Instruções

### Linha de Comando

#### Compilar o projeto

- Entre na pasta do projeto pelo terminal
- Execute os comandos:

```sh
javac -d . src/ds2/**/*.java
jar cvmf META-INF/MANIFEST.MF ed2.jar ds2
```

> Caso apareça a mensagem abaixo apenas ignore

```
Note: Some input files use unchecked or unsafe operations.
Note: Recompile with -Xlint:unchecked for details.
```

#### Executar

- Após ter compilado o projeto, ainda dentro da pasta do projeto, pelo terminal execute:

```sh
java -jar ed2.jar
```

- Irá abrir uma interface solicitando o arquivo `brazil_covid19_cities.csv`

> Está disponível no arquivo `data-demo.csv` um pequeno número de registros para testes rápidos

#### Executável Pré-compilado

Release v1.0:

- [Link](https://gitlab.com/btd1337/ed_2/-/releases/v1.0)

- [Executável](https://gitlab.com/btd1337/ed_2/uploads/29a036810cf3fdb44e9054805fa0f040/ed2.jar)
