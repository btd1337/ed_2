package ds2.utils;
import ds2.enums.Order;
import ds2.models.CovidCitiesData;
import ds2.sorting.Mergesort;
import ds2.sorting.Quicksort;
import ds2.sorting.Shellsort;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.util.Set;

public class Handlers {
	
	public static Date stringToDate(String date) throws ParseException {
		Date formattedDate = new SimpleDateFormat("yyyy-MM-dd").parse(date);
		
		return formattedDate;
	}
	
	public static String dateToString(Date date) {
		DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		
		return dateFormat.format(date);
	}
	
	/*
	 * Este método sorteia um sub-conjunto dentro de uma lista de entrada
	 * 
	 * Parâmetros:
	 * 
	 * books: Lista de elementos que serão sorteados
	 * size: Número de elementos a serem sorteados
	 * seed: Uma semente a ser utilizada no gerador de números aleatórios
	 * 
	 * Retorno: Um subconjunto da lista de entrada
	 * 
	 */
	public static <T> ArrayList<T> getRandomSubset(List<T> items, int size, int seed) {
		ArrayList<T> selectedItems = new ArrayList<T>();
		ArrayList<Integer> indexes = Handlers.getRandomNumbersArray(size, 0, items.size()-1, seed);

		for (Integer element:
				indexes) {
			selectedItems.add(items.get(element));
		}

		return selectedItems;
	}

	public static ArrayList<Integer> getRandomNumbersArray(int numElements, int min, int max, int seed) {
		Random randomIndex = new Random(seed);
		Set<Integer> indexes = new HashSet<Integer>();
		ArrayList<Integer> generatatedNumbers = new ArrayList<>();

		if (min > max) {
			int aux = min;
			min = max;
			max = aux;
		}

		// A utilização da estrutura Set garante que items repetidos não sejam adicionados
		while (indexes.size() < numElements) {
			indexes.add((randomIndex.nextInt(max - min + 1) + min));
		}

		for (int index : indexes) {
			generatatedNumbers.add(index);
		}

		return generatatedNumbers;
	}

	public static float getStatTime(Stats stats){
		String aux = stats.getProcessTime().replace("S", "");
		if(aux.contains("M")){
			String before = aux.split("M")[0];
			String after = aux.substring(aux.lastIndexOf("M") + 1);
			Float begin = Float.parseFloat(before) * 60;
			Float end = Float.parseFloat(after);
			return begin + end;
		} else{
			return Float.parseFloat(aux);
		}
	}

	/**
	 * Aplica os algoritmos de ordenação e escreve no arquivo saida.txt
	 * @param list
	 * @param numElements
	 * @param numSets
	 * @param outputFileName
	 */
	public static void comparateMSetsOfNElements(ArrayList<CovidCitiesData> list, int numElements, int numSets, String outputFileName) {
		int seed;
		File output = new File(outputFileName);
		FileWriter fileWriter;
		FileWriter writer = null;
		DecimalFormat df = new DecimalFormat();
		df.setMaximumFractionDigits(4);
		Double[] averageComparisons = {0.0, 0.0, 0.0};
		Double[] averageSwaps = {0.0, 0.0, 0.0};
		Float[] averageTime = {0.0f, 0.0f, 0.0f};

		try {
			writer = new FileWriter(output);
			writer.write("ESTATÍSTICAS:\nNum de elementos - " + numElements + "\n");

			System.out.printf("Elements: %d\n\n", numElements);
			for(int i = 0; i < numSets; i++){
				seed = i+1;
				writer.write("\n\nSemente: " + seed + "\n");
				System.out.printf("Semente: %d\n", (i+1));

				ArrayList<CovidCitiesData> subSetOriginal = getRandomSubset(list, numElements, seed);
				ArrayList<CovidCitiesData> subSet = new ArrayList<>(subSetOriginal);

				System.out.printf("== Sorting Quicksort...");
				Stats stats = new Stats();
				Quicksort.optimizedSort(subSet,
						0,
						subSet.size() - 1,
						CovidCitiesData.compareByTotalCases(),
						Order.ASC,
						stats,
						seed);
				stats.finishComparation();

				averageSwaps[0] += stats.getNumSwaps();
				averageComparisons[0] += stats.getNumComparations();
				averageTime[0] += getStatTime(stats);

				System.out.printf(" Writing results...\n");
				writer.write("Quicksort - " + stats.toString() + "\n");

				subSet = new ArrayList<>(subSetOriginal);

				System.out.printf("== Sorting Merge Sort...");
				stats = new Stats();
				Mergesort.sort(subSet,
						0,
						subSet.size(),
						CovidCitiesData.compareByTotalCases(),
						Order.ASC,
						stats);
				stats.finishComparation();
				System.out.printf(" Writing results...\n");
				writer.write("Mergesort - " + stats.toString() + "\n");

				averageSwaps[1] += stats.getNumSwaps();
				averageComparisons[1] += stats.getNumComparations();
				averageTime[1] += getStatTime(stats);

				subSet = new ArrayList<>(subSetOriginal);

				System.out.printf("== Sorting Shell Sort...");
				stats = new Stats();
				Shellsort.sort(subSet,
						CovidCitiesData.compareByTotalCases(),
						Order.ASC,
						stats);
				stats.finishComparation();
				System.out.printf(" Writing results...\n\n");
				writer.write("Shellsort - " + stats.toString() + "\n");

				averageSwaps[2] += stats.getNumSwaps();
				averageComparisons[2] += stats.getNumComparations();
				averageTime[2] += getStatTime(stats);
			}

			writer.write("\n=== Médias ===\n");
			writer.write("Número de comparações\n");
			writer.write("Quicksort - " + (averageComparisons[0]/numSets) + "\n");
			writer.write("Merge sort - " + (averageComparisons[1]/numSets) + "\n");
			writer.write("Shell sort - " + (averageComparisons[2]/numSets) + "\n");

			writer.write("\nNúmero de Trocas\n");
			writer.write("Quicksort - " + (averageSwaps[0]/numSets) + "\n");
			writer.write("Merge sort - " + (averageSwaps[1]/numSets) + "\n");
			writer.write("Shell sort - " + (averageSwaps[2]/numSets) + "\n");

			writer.write("\nTempo\n");
			writer.write("Quicksort - " + df.format((averageTime[0]/numSets)) + "\n");
			writer.write("Merge sort - " + df.format((averageTime[1]/numSets)) + "\n");
			writer.write("Shell sort - " + df.format((averageTime[2]/numSets)) + "\n");
			writer.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static int medianOfThreeRandomized(int lowestIndex, int highestIndex, int seed) {
		ArrayList<Integer> indexes = Handlers.getRandomNumbersArray(3, lowestIndex, highestIndex, seed);
		return Math.max(Math.min(indexes.get(0), indexes.get(1)), Math.min(Math.max(indexes.get(0), indexes.get(1)), indexes.get(2)));
	}
}