package ds2.sorting;

import ds2.enums.Order;
import ds2.utils.Handlers;
import ds2.utils.Stats;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class Quicksort {
	public static int insertSortFactor = 10;

	public static <T> void optimizedSort(List<T> items,
	                                     int lowestIndex,
	                                     int highestIndex,
	                                     Comparator comparator,
	                                     Order order,
	                                     Stats stats,
	                                     int seed) {
		while (lowestIndex < highestIndex) {
			if (highestIndex - lowestIndex <= insertSortFactor) {
				InsertionSort.sort(items, lowestIndex, highestIndex,comparator,order,stats);
				break;
			} else {
				int pivot = partition(items, lowestIndex, highestIndex, comparator, order, stats, seed);

				// Otimizações de recursão de cauda.
				if (pivot - lowestIndex < highestIndex - pivot) {
					optimizedSort(items, lowestIndex, pivot - 1, comparator, order, stats, seed);
					lowestIndex = pivot + 1;
				} else {
					optimizedSort(items, pivot + 1, highestIndex, comparator, order, stats, seed);
					highestIndex = pivot - 1;
				}
			}
		}
	}

	public static <T> void sort(List<T> items,
	                            int lowestIndex,
	                            int highestIndex,
	                            Comparator comparator,
	                            Order order,
	                            Stats stats,
	                            int seed) {
		while (lowestIndex < highestIndex) {

			int pivot = partition(items, lowestIndex, highestIndex, comparator, order, stats, seed);

			// Otimizações de recursão de cauda.
			if ((pivot - lowestIndex) < (highestIndex - pivot)) {
				sort(items, lowestIndex, pivot - 1, comparator, order, stats, seed);
				lowestIndex = pivot + 1;
			} else {
				sort(items, pivot + 1, highestIndex, comparator, order, stats, seed);
				highestIndex = pivot - 1;

			}
		}
	}

	private static <T> int partition(List<T> items, int lowestIndex, int highestIndex, Comparator comparator, Order order, Stats stats, int seed) {
		int pivotIndex = Quicksort.determinePivot(items,lowestIndex,highestIndex, seed);
		T pivot = items.get(pivotIndex);
		int i = lowestIndex - 1;
		for (int j = lowestIndex; j < highestIndex; j++) {
			stats.addNumComparations();

			// Determina se a ordenação será ascendente ou descendente
			if (order.getValue() * comparator.compare(items.get(j), pivot) <= 0) {
				i++;
				swap(items, i, j, stats);
			}
		}

		swap(items, i+1, pivotIndex, stats);

		return i + 1;
	}

	private static <T> void swap(List<T> items, int value1Index, int value2Index, Stats stats) {
		T temp = items.get(value1Index);
		items.set(value1Index, items.get(value2Index));
		items.set(value2Index, temp);
		stats.addNumSwaps();
	}

	/**
	 * Determina o pivot com base em uma mediana de 3 aleatória
	 * @param items
	 * @param lowestIndex
	 * @param highestIndex
	 * @param seed
	 * @return elemento mediano aleatório
	 */
	private static <T> int determinePivot(List<T> items, int lowestIndex, int highestIndex, int seed) {

		if (highestIndex - lowestIndex < 3) {
			return highestIndex > lowestIndex ? highestIndex : lowestIndex;
		}

		return Handlers.medianOfThreeRandomized(lowestIndex, highestIndex, seed);
	}
}

