package ds2.sorting;

import ds2.enums.Order;
import ds2.utils.Stats;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class Mergesort {

    public static <T> void sort(List<T> list, int start, int end, Comparator comparator, Order order, Stats stats){
        if(start < (end-1)){
            int mid = (start + end) / 2;
            sort(list, start, mid, comparator, order, stats);
            sort(list, mid, end, comparator, order, stats);
            merge(list, start, mid, end, comparator, order, stats);
        }
    }

    private static <T> void merge(List<T> list, int start, int mid, int end, Comparator comparator, Order order, Stats stats){
        List<T> aux;
        aux = new ArrayList<T>();
        int i = start;
        int j = mid;
        while((i < mid) && (j < end)){
            if(order.getValue() * comparator.compare(list.get(i), list.get(j)) <= 0){
                aux.add(list.get(i));
                i++;
            } else{
                aux.add(list.get(j));
                j++;
            }
            stats.addNumComparations();
        }
        while(i < mid) {aux.add(list.get(i)); i++;}
        while(j < end) {aux.add(list.get(j)); j++;}

        for(int n = start; n < end; n++){
            list.set(n, aux.get(n-start));
            stats.addNumSwaps();
        }
    }
}
