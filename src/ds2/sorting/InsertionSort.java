package ds2.sorting;

import ds2.enums.Order;
import ds2.utils.Stats;

import java.util.Comparator;
import java.util.List;

public class InsertionSort {
	public static <T> void sort(List<T> items,
								int lowestIndex,
	                            int highestIndex,
	                            Comparator comparator,
	                            Order order,
	                            Stats stats) {
		for (int i = lowestIndex; i <= highestIndex; i++){

			T aux = items.get(i);
			int j = i;

			while ((j > 0) && ((order.getValue() * comparator.compare(items.get(j-1), aux) > 0))){
				items.set(j, items.get(j-1));
				stats.addNumComparations();
				stats.addNumSwaps();
				j--;
			}
			items.set(j, aux);
		}
	}
}
