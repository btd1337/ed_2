package ds2.sorting;

import ds2.enums.Order;
import ds2.utils.Stats;
import java.util.Comparator;
import java.util.List;

public class Shellsort {
    public static <T> void sort(List<T> list, Comparator comparator, Order order, Stats stats) {
        int size = list.size();

        for (int gap = size / 2; gap > 0; gap /= 2) {
            for (int i = gap; i < size; i++) {
                T key = list.get(i);
                int j = i;
                while (j >= gap && order.getValue() * comparator.compare(list.get(j - gap), key) >= 0) {
                    list.set(j, list.get(j-gap));
                    stats.addNumSwaps();
                    stats.addNumComparations();
                    j -= gap;
                }
                list.set(j, key);
            }
        }
    }
}
