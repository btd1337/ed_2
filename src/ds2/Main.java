package ds2;

import ds2.enums.Order;
import ds2.models.CovidCitiesData;
import ds2.sorting.Mergesort;
import ds2.sorting.Quicksort;
import ds2.sorting.Shellsort;
import ds2.utils.Handlers;
import ds2.utils.IOFile;
import ds2.utils.Stats;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		File fileCovidCitiesData;
		ArrayList<CovidCitiesData> covidCitiesData;
		String fileOutputStep1 = "brazil_covid19_cities_processado";
		String fileOutputStep2 = "saida.txt";
		int[] numElements = {10000};
		int numSets = 5;
		int option;
		Scanner scanner = new Scanner(System.in);

		Main.introduction();

		// Leitura do arquivo de entrada
		try {
			fileCovidCitiesData = IOFile.getFileFromDialog();
			System.out.printf("Lendo arquivo %s...\n", fileCovidCitiesData.getName());
			covidCitiesData = IOFile.readObjectsFromCSV(fileCovidCitiesData.getAbsolutePath(), true, CovidCitiesData.class);
		} catch (FileNotFoundException e) {
			System.out.println("Erro: Arquivo requerido não encontrado!");
			return ;
		}

		do {
			System.out.printf("\nEscolha a opção que deseja executar:\n");
			System.out.println("1- Etapa 1");
			System.out.println("2- Etapa 2");
			System.out.println("3- Módulo de Testes");
			System.out.println("0- Sair");

			try {
				option = scanner.nextInt();
			} catch (InputMismatchException e) {
				scanner.next();
				System.out.println("Erro: Valor inválido");
				option = -1;
			}

			switch (option) {
				case 1:
					Main.step1(covidCitiesData, fileOutputStep1);
					break;
				case 2:
					Main.step2(covidCitiesData, numElements, numSets, fileOutputStep2);
					break;
				case 3:
					Main.testModule(covidCitiesData);
					break;
				case 0:
					System.out.printf("\nSaindo...\n");
					return;
				default:
					System.out.printf("Opção inválida\n\n");
					break;
			}
		} while (option != 0);
	}

	private static void introduction() {
		System.out.println("=== TRABALHO DE ED2 ===");
		System.out.println("Por favor, selecione o arquivo de entrada.");
	}

	/**
	 * Realiza ordenação pelo par Estado-Cidade e depois a data
	 * // TODO Após a ordenação, você deverá transformar os totais acumulados de casos em totais diários
	 * @param covidCitiesData
	 * @param outputFile
	 */
	private static void step1(ArrayList<CovidCitiesData> covidCitiesData, String outputFile) {
		System.out.println("Ordenando...");
		Stats stats = new Stats();
		Mergesort.sort(
				covidCitiesData,
				0,
				covidCitiesData.size(),
				CovidCitiesData.compareByStateAndCityThenDate(),
				Order.ASC,
				stats
		);
		stats.finishComparation();
		System.out.println(stats.toString());

		System.out.printf("Calculando os totais diários... ");
		Main.transformAccumulatedTotalToDailyTotal(covidCitiesData);
		System.out.printf("Feito.\n");
		try {
			IOFile.writeObjectsToCSV(covidCitiesData, outputFile);
		} catch (IOException e) {
			e.printStackTrace();
			return ;
		}
	}

	/**
	 * Etapa 2
	 * @param covidCitiesData
	 * @param numElements
	 * @param numSets
	 * @param fileName
	 */
	private static void step2(ArrayList<CovidCitiesData> covidCitiesData, int[] numElements, int numSets, String fileName) {
		System.out.println("Ordenando...");
		for(int i = 0; i < numElements.length; i++) {
			Handlers.comparateMSetsOfNElements(covidCitiesData, numElements[i], numSets, fileName);
		}
	}

	/**
	 * Modulo de teste
	 */
	private static void testModule(ArrayList<CovidCitiesData> list) {
		int seedMedian = 1;
		String outputFileName = "saida-modulo-teste.txt";
		Scanner sc = new Scanner(System.in);
		System.out.println("Você está no módulo de testes. Digite \"1\" para a saída em arquivo" +
				" e \"2\" para a saída no console.");
		int inputNumber = sc.nextInt();
		System.out.println(inputNumber);
		if(inputNumber == 1) {
			Handlers.comparateMSetsOfNElements(list, 100, 1, outputFileName);
		} else if (inputNumber == 2){
			ArrayList<CovidCitiesData> subSetOriginal = Handlers.getRandomSubset(list, 10, 1);
			ArrayList<CovidCitiesData> subSet = new ArrayList<>(subSetOriginal);
			Stats stats = new Stats();
			Quicksort.optimizedSort(subSet, 0, subSet.size() - 1,
					CovidCitiesData.compareByTotalCases(), Order.ASC, stats, seedMedian);
			stats.finishComparation();
			System.out.println("\nQuicksort: " + stats.toString());
			for(CovidCitiesData elt: subSet){
				System.out.println(elt.toString());
			}
			subSet = new ArrayList<>(subSetOriginal);
			stats = new Stats();
			Mergesort.sort(subSet, 0, subSet.size(),
					CovidCitiesData.compareByTotalCases(), Order.ASC, stats);
			stats.finishComparation();
			System.out.println("\nMergesort: " + stats.toString());
			for(CovidCitiesData elt: subSet){
				System.out.println(elt.toString());
			}
			subSet = new ArrayList<>(subSetOriginal);
			stats = new Stats();
			Shellsort.sort(subSet,
					CovidCitiesData.compareByTotalCases(), Order.ASC, stats);
			stats.finishComparation();
			System.out.println("\nShellsort: " + stats.toString());
			for(CovidCitiesData elt: subSet){
				System.out.println(elt.toString());
			}
		} else{
			System.out.println("Opção inválida\n");
		}
	}

	private static void transformAccumulatedTotalToDailyTotal(ArrayList<CovidCitiesData> covidCitiesData) {
		for (int i = 2; i < covidCitiesData.size(); i++) {
			if (covidCitiesData.get(i).getState().equals(covidCitiesData.get(i-1).getState()) && covidCitiesData.get(i).getName().equals(covidCitiesData.get(i-1).getName())) {
				covidCitiesData.get(i).setCases(covidCitiesData.get(i).getCases() - covidCitiesData.get(i - 1).getCases());
				covidCitiesData.get(i).setDeaths(covidCitiesData.get(i).getDeaths() - covidCitiesData.get(i - 1).getDeaths());
			}
		}
	}
}
